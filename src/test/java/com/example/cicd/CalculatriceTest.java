package com.example.cicd;

import org.junit.jupiter.api.*;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {

    private  Calculatrice calculatrice;

    @BeforeEach
    void setUp() {
        this.calculatrice = new Calculatrice();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName(("L'addition doit fonctionner"))
    void testAdditionner() {
        assertEquals(15, calculatrice.additionner(10,5), "L'addition de 10 et 5 a bien fonctionne");
    }

    @Test
    @DisplayName("La multiplication doit fonctionner")
    void multiplier() {
        assertEquals(15, calculatrice.multiplier(3,5), "La multiplication de 5 et 3 doit donner 15");
    }

    @Test
    @DisplayName("La multiplication par zero doit donner zero")
    void multiplierParZero(){
        assertEquals(0,calculatrice.multiplier(42,0), "42 * 0 = 0!");
    }

    @Test
    @DisplayName("La division par zero doit echouer")
    void diviserParZero() {
        assertThrows(ArithmeticException.class, ()-> calculatrice.diviser(4,0), "La division par zero leve une erreur");
    }

    @RepeatedTest(5)
    @DisplayName("La division doit fonctionner plusieurs fois")
    void diviserTest(){
        assertEquals(4,calculatrice.diviser(20,5), "20 / 5 = 4!");
    }

    @Test
    void testGroupe(){
        Dimension rect = new Dimension(300,500);
        assertAll("On teste les dimensions du rectangle",
                ()-> {assertEquals(500.00, rect.getHeight(), "La hauteur est egale a 500");},
                ()->{assertTrue(rect.getWidth() == 300, "La largeur doit etre egale a 400");});
    }

//    @Test
//    void checkLists(){
//        List<String> liste = Arrays.asList("jean@mail.com", "marie@mail.com");
//        List<String > liste2 = Arrays.asList("jean@mail.com", "maria@mail.com");
//        List<String> liste3 = Arrays.asList("(.*)@(.*)", "(.*)@(.*)");
//        assertAll("les listes doivent correspondre", () -> {
//            assertLinesMatch(liste2,liste, "Les listes ne correspondent pas");
//        }, () -> {assertLinesMatch(liste3,liste2,"Les listes correspondent" );});
//    }
}